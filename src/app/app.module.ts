import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { NavComponent } from './nav/nav.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatToolbarModule} from '@angular/material/toolbar';
import { GridviewComponent } from './gridview/gridview.component';
import {MatGridListModule} from '@angular/material/grid-list';
import { Routes, RouterModule } from '@angular/router';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSelectModule} from '@angular/material/select';
import {MatChipsModule} from '@angular/material/chips';
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import {environment} from '../environments/environment';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatRadioModule} from '@angular/material/radio';
import { TestingComponent } from './testing/testing.component';
import {MatCardModule} from '@angular/material/card';
import { VideosComponent } from './videos/videos.component';
import { MatVideoModule } from 'mat-video';
import { HttpClientModule } from '@angular/common/http';
import { EmbedVideo } from 'ngx-embed-video';
import { CategoriesComponent } from './categories/categories.component';
import { Category1Component } from './category1/category1.component';
import { Category2Component } from './category2/category2.component';
import { Category3Component } from './category3/category3.component';
import { Category4Component } from './category4/category4.component';
@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    RegisterComponent,
    LoginComponent,
    NavComponent,
    GridviewComponent,
    TestingComponent,
    VideosComponent,
    CategoriesComponent,
    Category1Component,
    Category2Component,
    Category3Component,
    Category4Component
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatGridListModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatChipsModule,
    AngularFireModule.initializeApp(environment.firebase),   
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    MatCheckboxModule,
    MatRadioModule,
    MatCardModule,
    MatVideoModule,
    HttpClientModule,
    EmbedVideo.forRoot(),
      



    RouterModule.forRoot([
      {path:'',component: RegisterComponent},
      {path:'testing',component: TestingComponent},
      {path:'welcome',component: WelcomeComponent},
      {path:'videos',component: VideosComponent},
      {path:'categories',component: CategoriesComponent},
      {path:'category1',component: Category1Component},
      {path:'category2',component: Category2Component},
      {path:'category3',component: Category3Component},
      {path:'category4',component: Category4Component},
      {path:'**',component: LoginComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
