import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';

@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  dogs = [];
  groups = [];

  // addTodo(text:string){
  //   this.authService.user.subscribe(user=>{
  //     this.db.list('/users/'+user.uid+'/todos').push({'text':text});
  //   })
  // }

 

  constructor( private db:AngularFireDatabase ) { }

  ngOnInit() {

    this.db.list('/כלבים').snapshotChanges().subscribe(
      dogs =>
      {
        this.dogs = [];
       // this.dogs.push(dogs);
        
        dogs.forEach(
          dog =>
          {

            let m = dog.payload.toJSON();
            // m['key'] = dog.key;
            this.dogs.push(m);

          }
        )
        console.log(this.dogs);
      }
    )


    this.db.list('/קבוצות').snapshotChanges().subscribe(
      groups =>
      {
        this.groups = [];
       // this.dogs.push(dogs);
        
       groups.forEach(
          group =>
          {

            let m = group.payload.toJSON();
            // m['key'] = dog.key;
            this.groups.push(m);

          }
        )
        console.log(this.groups);
      }
    )

  }

}
